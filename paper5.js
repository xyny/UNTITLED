// Set variable defaults

var baseSize = 100;
var spacingOffset = 0;
var circleRecursion = 10;
var animationType = 'random';
var randomnessRange = 0.01;
var growShrinkFactor = 1.01;

var spacing;
var tile;

var tileBy = Math.max(view.viewSize.width, view.viewSize.height);

initUI();

draw();

function draw() {
    project.activeLayer.removeChildren();

    spacing = baseSize*2+spacingOffset;
    tile = tileBy/spacing+1;

    for (var x = 0; x<tile; x++) {
        for (var y = 0; y<tile; y++) {
            drawCircles(x,y,baseSize,circleRecursion)
        }
    }
}

function getRandomBetween(min, max) {
    return Math.random() * (max - min) + min;
  }
  

function drawCircles(x, y, size, circleRecursion) {
    var path = Path.Circle(new Point(x*spacing, y*spacing), size);
    path.strokeColor = 'black';

    path.onFrame = function() {
        if (animationType == 'random') {
            path.scale(getRandomBetween(1-randomnessRange,1+randomnessRange));
        }
        else if (animationType == 'grow-shrink') {
            path.scale(growShrinkFactor);
        }
    }

    if (circleRecursion == 0) { return; }

    drawCircles(x,y,size-10,circleRecursion-1);
}

function initUI() {
    var sizeSlider = document.getElementById("sizeSlider");
    sizeSlider.value = baseSize;
    sizeSlider.oninput = function() {
        baseSize = this.valueAsNumber;
        draw();
    }

    var spacingOffsetSlider = document.getElementById("spacingOffsetSlider");
    spacingOffsetSlider.value = spacingOffset;
    spacingOffsetSlider.oninput = function() {
        spacingOffset = this.valueAsNumber;
        draw();
    }

    var circleRecursionSlider = document.getElementById("circleRecursionSlider");
    circleRecursionSlider.value = circleRecursion;
    circleRecursionSlider.oninput = function() {
        circleRecursion = this.valueAsNumber;
        draw();
    }

    var animationTypeDropdown = document.getElementById("animationTypeDropdown");
    animationTypeDropdown.value = animationType;
    animationTypeDropdown.oninput = function() {
        animationType = this.value;
    }

    var randomnessRangeSlider = document.getElementById("randomnessRangeSlider");
    randomnessRangeSlider.value = randomnessRange*100;
    randomnessRangeSlider.oninput = function() {
        randomnessRange = this.valueAsNumber/100;
    }

    var growShrinkFactorSlider = document.getElementById("growShrinkFactorSlider");
    growShrinkFactorSlider.value = growShrinkFactor;
    growShrinkFactorSlider.oninput = function() {
        growShrinkFactor = this.valueAsNumber;
    }



    var settingsMenu = document.getElementById("settings");
    settingsMenu.className = 'settings closed';

    var settingsToggle = document.getElementById("settingsToggle");

    var settingsOpenButton = document.getElementById("settingsOpenButton");
    settingsOpenButton.onclick = function() {
        settingsMenu.className = 'settings open';
        settingsToggle.className = 'settingsToggle closed'
    }

    var settingsCloseButton = document.getElementById("settingsCloseButton");
    settingsCloseButton.onclick = function() {
        settingsMenu.className = 'settings closed';
        settingsToggle.className = 'settingsToggle open'
    }


    var redrawButton = document.getElementById("redrawButton");
    redrawButton.onclick = function(){ draw() }; // Somehow this works but just draw() without the anonymous func didn't.
} 