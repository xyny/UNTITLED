var path = new Path({
    strokeColor: 'black',
    strokeWidth: 10,
});

onMouseDrag = function(event) {
    path.add(event.point);
}

var shadows = [];

onMouseUp = function() {
    path.simplify();
    for (var i = 0; i < 50; i++) {
        var shadow = path.clone();
        shadow.sendToBack();
        shadow.translate(new Point(10*i,10*i))
        shadow.opacity = .5/i;
        shadows.push(shadow);
    }
}

onMouseDown = function() {
    path = new Path({
        strokeColor: 'black',
        strokeWidth: 10,
    });
    // for (var i = 0; i < shadows.length; i++) {
    //     shadows[i].remove();
    // }
}