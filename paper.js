var points = new Path();

for (var i = 0; i<100; i++) {
    var point = Point.random() * new Point(view.viewSize.width,view.viewSize.height);
    points.add(point);
    // var circle = Path.Circle(point, 10);
    // circle.fillColor = 'black';
}

var path = new Path();
path.strokeColor = 'black';
path.strokeWidth = 10;

drawPoints(points.segments[0]);

function drawPoints(segment) {
    var point = segment.point;

    // Add if new line doesn't intersect, moveTo if does.
    // if (path.lastSegment) {
    //     var testpath = new Path([path.lastSegment.point, point]);
    //     if (path.getCrossings(testpath).length != 0) {
    //         console.log(path.getCrossings(testpath));
    //         path = path.splitAt(segment);
    //         path.add(point);
    //     } else {
    //         path.add(point);
    //     }
    // } else {
        path.add(point);
    // }
    
    // Remove the used segment
    points.removeSegment(segment.index);

    // Loop again
    var nextPoint = points.getNearestPoint(point);
    var nextLocation = points.getLocationOf(nextPoint);
    if (!nextLocation) {
        return
    }
    drawPoints(nextLocation.segment);
}

path.smooth({type: 'catmull-rom', factor: 0.5});